#! /bin/sh
route add -net 0.0.0.0 netmask 0.0.0.0 gw $ifconfig_remote dev tun0
route add -net 0.0.0.0 netmask 128.0.0.0 gw $ifconfig_remote dev tun0
route add -net 128.0.0.0 netmask 128.0.0.0 gw $ifconfig_remote dev tun0
/etc/openvpn/update-resolv-conf
#needs to be killed separately when vpn goes down
./loopForward.sh&
