#! /bin/sh

. ./local-configPiaCreds.sh

currentPort=`curl -d "user=${piaUser}&pass=${piaPass}&client_id=${piaClientId}&local_ip=$1"   https://www.privateinternetaccess.com/vpninfo/port_forward_assignment | egrep -o [[:digit:]]+`

echo New forwarding port: $currentPort
transmission-remote -p $currentPort
transmission-remote --reannounce
