## Requirements ##
install openvpn
requires network-manager for shutting down network if vpn fails
pia.crt file in script dir
pia.creds.txt file in script dir


## Behaviors ##
Routes all traffic through VPN
Uses VPN-configured DNS servers
Shuts down network-manager when VPN terminates
Stores username/pw/cert in separate files


## Running ##
Run launch.sh. It will sudo launch open vpn with the pia.conf in the same directory.
