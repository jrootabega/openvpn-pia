#! /bin/sh

. ./local-configVpnData.sh || exit


sudo iptables -D VPNWHITE 10
sudo iptables -D VPNWHITE 9
sudo iptables -D VPNWHITE 8
sudo iptables -D VPNWHITE 7
sudo iptables -D VPNWHITE 6
sudo iptables -D VPNWHITE 5
sudo iptables -D VPNWHITE 4
sudo iptables -D VPNWHITE 3
sudo iptables -D VPNWHITE 2
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -D VPNWHITE 1
sudo iptables -X VPNWHITE

sudo iptables -N VPNWHITE

sudo iptables -I VPNWHITE 1 -p tcp \! --dport $vpnPortTcp -j DROP 
sudo iptables -I VPNWHITE 1 -p tcp \! -d $white1 -j DROP 
sudo iptables -I VPNWHITE 1 -p tcp \! -d $white2 -j DROP 
sudo iptables -I VPNWHITE 1 -p tcp \! -d $white3 -j DROP 
sudo iptables -I VPNWHITE 1 -p tcp \! -d $white4 -j DROP 
sudo iptables -I VPNWHITE 1 -p tcp \! -d $white5 -j DROP 
sudo iptables -I VPNWHITE 1 -p tcp \! -d $white6 -j DROP 
sudo iptables -I VPNWHITE 1 -p tcp \! -d $white7 -j DROP 

sudo iptables -I VPNWHITE 1 -p udp \! --dport $vpnPortUdp  -j DROP 
sudo iptables -I VPNWHITE 1 -p udp \! -d $white1 -j DROP 
sudo iptables -I VPNWHITE 1 -p udp \! -d $white2 -j DROP 
sudo iptables -I VPNWHITE 1 -p udp \! -d $white3 -j DROP 
sudo iptables -I VPNWHITE 1 -p udp \! -d $white4 -j DROP 
sudo iptables -I VPNWHITE 1 -p udp \! -d $white5 -j DROP 
sudo iptables -I VPNWHITE 1 -p udp \! -d $white6 -j DROP 
sudo iptables -I VPNWHITE 1 -p udp \! -d $white7 -j DROP 

sudo iptables -I VPNWHITE 1 -p udp --dport 53 -d $normalGateway -j ACCEPT 


